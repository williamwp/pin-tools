
**Rendering arithmatic supported**

| name    | description             | status                    |
| ------- | ---------------- | ----------------------- |
| ray tracing    | info         | :ballot_box_with_check: |
| path tracing  | info     | :ballot_box_with_check: |
| fractal | info | :ballot_box_with_check: |
| PPM  | info     | :ballot_box_with_check: |

